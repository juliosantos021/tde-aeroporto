﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DCE_Aeroporto.Dados;
using DCE_Aeroporto.Models;

namespace DCE_Aeroporto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PassageirosVoosController : ControllerBase
    {
        private readonly DCE_AeroportoContext _context;

        public PassageirosVoosController(DCE_AeroportoContext context)
        {
            _context = context;
        }

        // GET: api/PassageirosVoos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PassageiroVoo>>> GetPassageiroVoo()
        {
            return await _context.PassageiroVoo.ToListAsync();
        }

        // GET: api/PassageirosVoos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PassageiroVoo>> GetPassageiroVoo(int id)
        {
            var passageiroVoo = await _context.PassageiroVoo.FindAsync(id);

            if (passageiroVoo == null)
            {
                return NotFound();
            }

            return passageiroVoo;
        }

        // PUT: api/PassageirosVoos/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPassageiroVoo(int id, PassageiroVoo passageiroVoo)
        {
            if (id != passageiroVoo.CPF)
            {
                return BadRequest();
            }

            _context.Entry(passageiroVoo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PassageiroVooExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PassageirosVoos
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<PassageiroVoo>> PostPassageiroVoo(PassageiroVoo passageiroVoo)
        {
            _context.PassageiroVoo.Add(passageiroVoo);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PassageiroVooExists(passageiroVoo.CPF))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPassageiroVoo", new { id = passageiroVoo.CPF }, passageiroVoo);
        }

        // DELETE: api/PassageirosVoos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PassageiroVoo>> DeletePassageiroVoo(int id)
        {
            var passageiroVoo = await _context.PassageiroVoo.FindAsync(id);
            if (passageiroVoo == null)
            {
                return NotFound();
            }

            _context.PassageiroVoo.Remove(passageiroVoo);
            await _context.SaveChangesAsync();

            return passageiroVoo;
        }

        private bool PassageiroVooExists(int id)
        {
            return _context.PassageiroVoo.Any(e => e.CPF == id);
        }
    }
}
