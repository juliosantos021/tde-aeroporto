﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DCE_Aeroporto.Dados;
using DCE_Aeroporto.Models;

namespace DCE_Aeroporto.Controllers
{
    [Route("api/angar/avioes")]
    [ApiController]
    public class AvioesController : ControllerBase
    {
        private readonly DCE_AeroportoContext _context;

        public AvioesController(DCE_AeroportoContext context)
        {
            _context = context;
        }

        // GET: api/Avioes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Aviao>>> GetAvioes()
        {
            return await _context.Avioes.ToListAsync();
        }

        // GET: api/Avioes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Aviao>> GetAviao(int id)
        {
            var aviao = await _context.Avioes.FindAsync(id);

            if (aviao == null)
            {
                return NotFound();
            }

            return aviao;
        }

        // PUT: api/Avioes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAviao(int id, Aviao aviao)
        {
            if (id != aviao.Id)
            {
                return BadRequest();
            }

            _context.Entry(aviao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AviaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Avioes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Aviao>> PostAviao(Aviao aviao)
        {
            _context.Avioes.Add(aviao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAviao", new { id = aviao.Id }, aviao);
        }

        // DELETE: api/Avioes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Aviao>> DeleteAviao(int id)
        {
            var aviao = await _context.Avioes.FindAsync(id);
            if (aviao == null)
            {
                return NotFound();
            }

            _context.Avioes.Remove(aviao);
            await _context.SaveChangesAsync();

            return aviao;
        }

        private bool AviaoExists(int id)
        {
            return _context.Avioes.Any(e => e.Id == id);
        }
    }
}
