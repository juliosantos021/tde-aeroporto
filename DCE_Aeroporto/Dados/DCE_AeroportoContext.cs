﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DCE_Aeroporto.Models;


namespace DCE_Aeroporto.Dados
{

        public class DCE_AeroportoContext : DbContext
        {
            public DCE_AeroportoContext(DbContextOptions<DCE_AeroportoContext> options) : base(options)
            {
            }


                protected override void OnModelCreating(ModelBuilder modelBuilder)
                {
                    modelBuilder.Entity<PassageiroVoo>()
                        .HasKey(pv => new { pv.CPF, pv.NumeroVoo });

                    modelBuilder.Entity<Voo>()
                        .HasKey(v => new { v.IdAviao });

                    modelBuilder.Entity<Passageiro>()
                        .HasKey(p => new { p.CPF });
                }



                public DbSet<Aviao> Avioes { get; set; }
                public DbSet<Passageiro> Passageiros { get; set; }
                public DbSet<Voo> Voos { get; set; }
                public DbSet<DCE_Aeroporto.Models.PassageiroVoo> PassageiroVoo { get; set; }
        }

}
