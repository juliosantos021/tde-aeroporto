﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DCE_Aeroporto.Migrations
{
    public partial class Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Avioes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Modelo = table.Column<int>(type: "integer", nullable: false),
                    Numero_Passageiros = table.Column<int>(type: "integer", nullable: false),
                    Potencia = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Avioes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Passageiros",
                columns: table => new
                {
                    CPF = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Nome = table.Column<string>(type: "text", nullable: true),
                    Data_Nascimento = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Passaporte = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Passageiros", x => x.CPF);
                });

            migrationBuilder.CreateTable(
                name: "Voos",
                columns: table => new
                {
                    IdAviao = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Horario = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Duracao = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Origem = table.Column<string>(type: "text", nullable: true),
                    Destino = table.Column<string>(type: "text", nullable: true),
                    NumeroVoo = table.Column<int>(type: "integer", nullable: false),
                    AviaoId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Voos", x => x.IdAviao);
                    table.ForeignKey(
                        name: "FK_Voos_Avioes_AviaoId",
                        column: x => x.AviaoId,
                        principalTable: "Avioes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PassageiroVoo",
                columns: table => new
                {
                    CPF = table.Column<int>(type: "integer", nullable: false),
                    NumeroVoo = table.Column<int>(type: "integer", nullable: false),
                    VooIdAviao = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PassageiroVoo", x => new { x.CPF, x.NumeroVoo });
                    table.ForeignKey(
                        name: "FK_PassageiroVoo_Passageiros_CPF",
                        column: x => x.CPF,
                        principalTable: "Passageiros",
                        principalColumn: "CPF",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PassageiroVoo_Voos_VooIdAviao",
                        column: x => x.VooIdAviao,
                        principalTable: "Voos",
                        principalColumn: "IdAviao",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PassageiroVoo_VooIdAviao",
                table: "PassageiroVoo",
                column: "VooIdAviao");

            migrationBuilder.CreateIndex(
                name: "IX_Voos_AviaoId",
                table: "Voos",
                column: "AviaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PassageiroVoo");

            migrationBuilder.DropTable(
                name: "Passageiros");

            migrationBuilder.DropTable(
                name: "Voos");

            migrationBuilder.DropTable(
                name: "Avioes");
        }
    }
}
