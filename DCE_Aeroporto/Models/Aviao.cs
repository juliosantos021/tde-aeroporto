﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DCE_Aeroporto.Models
{
    public class Aviao
    {
        public int Modelo { get; set; }
        public int Numero_Passageiros { get; set; }
        public int Potencia { get; set; }
        public int Id { get; set; }
        public List<Voo> Voos { get; set; }
    }
}
