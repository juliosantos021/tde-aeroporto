﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DCE_Aeroporto.Models
{
    public class Voo
    {
        public DateTime Horario { get; set; }
        public DateTime Duracao { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public int NumeroVoo { get; set; }
        public int IdAviao { get; set; }
        public Aviao Aviao { get; set; }
    }

}
