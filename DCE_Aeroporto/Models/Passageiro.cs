﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DCE_Aeroporto.Models
{
    public class Passageiro
    {
        public string Nome { get; set; }
        public DateTime Data_Nascimento { get; set; }
        
        public int CPF { get; set; }

        public int Passaporte { get; set; }
    }
}
