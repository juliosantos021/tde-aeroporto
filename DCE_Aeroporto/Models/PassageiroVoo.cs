﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DCE_Aeroporto.Models
{
    public class PassageiroVoo
    {
        public int CPF { get; set; }
        public Passageiro Passageiro { get; set; }
        public int NumeroVoo { get; set; }
        public Voo Voo { get; set; }
    }
}
